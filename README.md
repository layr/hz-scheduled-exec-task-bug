# hz-scheduled-exec-task-bug

Named tasks won't get spring beans `@Autowired` in.
Run the app to see the difference in stdout log:

a) add enterprise licence key in `application.yaml`

b) working configuration (not using NamedTasks):

`./mvnw clean spring-boot:run -Dspring.profiles.active=works`

c) non-working configuration (using NamedTasks):

`./mvnw clean spring-boot:run -Dspring.profiles.active=doesnt-work`
