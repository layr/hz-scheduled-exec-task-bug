package hz.bug.app.config;

import com.hazelcast.config.Config;
import com.hazelcast.config.JoinConfig;
import com.hazelcast.config.ScheduledExecutorConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.spring.context.SpringAware;
import com.hazelcast.spring.context.SpringManagedContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.Collections.singletonList;
import static java.util.Optional.ofNullable;

@Configuration
public class HzConf {

    public static final String HZ_SCHEDULER = "hzDummyTaskScheduler";

    @Autowired
    private ConfigProps configProps;

    @Bean
    public Config config() {
        Config c = new Config();

        JoinConfig joinConfig = c.getNetworkConfig().getJoin();
        joinConfig.getMulticastConfig().setEnabled(false);
        joinConfig.getTcpIpConfig().setEnabled(true).setMembers(singletonList("127.0.0.1"));

        c.setLicenseKey(configProps.getLicenceKey());

        c.setManagedContext(managedContext());
        setSchedulerConfig(c);
        return c;
    }

    @Bean
    public HazelcastInstance hz() {
        return Hazelcast.newHazelcastInstance(config());
    }

    /**
     * This allows us to use {@link SpringAware} annotation.
     */
    @Bean
    public SpringManagedContext managedContext() {
        return new SpringManagedContext();
    }

    private void setSchedulerConfig(final Config config) {
        ScheduledExecutorConfig conf = new ScheduledExecutorConfig(HZ_SCHEDULER);

        ofNullable(configProps.getCapacity()).ifPresent(conf::setCapacity);
        ofNullable(configProps.getPoolSize()).ifPresent(conf::setPoolSize);

        config.addScheduledExecutorConfig(conf);
    }
}
