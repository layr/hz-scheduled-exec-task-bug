package hz.bug.app.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Data
@ConfigurationProperties(prefix = "hz")
@Configuration
public class ConfigProps {

    private Long periodMs = 1000L;
    private Long initialDelayMs = 500L;
    private Integer capacity;
    private Integer poolSize = 1;

    private String licenceKey;
}
