package hz.bug.app.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Slf4j
public class DummyService {

    public void doSomething(final String txt) {
        log.info("i'm doing something: {}", txt);
    }

    @PostConstruct
    public void postConstruct() {
        log.info("Started {}", this.getClass().getSimpleName());
    }
}
