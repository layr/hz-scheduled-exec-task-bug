package hz.bug.app.service.scheduler;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.scheduledexecutor.IScheduledExecutorService;
import hz.bug.app.config.ConfigProps;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.annotation.PostConstruct;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static com.hazelcast.scheduledexecutor.TaskUtils.named;
import static hz.bug.app.config.HzConf.HZ_SCHEDULER;


@Slf4j
@Configuration
@RequiredArgsConstructor
public class DummyTaskScheduler {

    private static final String JOB_NAME = "dummyJob";

    private final HazelcastInstance hz;
    private final ConfigProps configProps;

    @Profile("works")
    @Configuration
    public class Works {

        @PostConstruct
        public void start() {
            log.info("running 'works' postconstruct");
            if (shouldScheduleJobBySchedulerName(HZ_SCHEDULER)) {
                log.info("scheduling job [{}] with executorService [{}]", JOB_NAME, HZ_SCHEDULER);
                getExecService().scheduleOnKeyOwnerAtFixedRate(
                        new DummyTask(),  // non-named task gets beans autowired as expected
                        JOB_NAME, configProps.getInitialDelayMs(),
                        configProps.getPeriodMs(), TimeUnit.MILLISECONDS
                );
            }
        }
    }

    @Profile("doesnt-work")
    @Configuration
    public class DoesNotWork {

        @PostConstruct
        public void start() {
            log.info("running 'doesnt-work' postconstruct");
            if (shouldScheduleJobByJobName(JOB_NAME)) {
                log.info("scheduling job [{}] with executorService [{}]", JOB_NAME, HZ_SCHEDULER);
                getExecService().scheduleOnKeyOwnerAtFixedRate(
                        named(JOB_NAME, new DummyTask()),  // NamedTask is buggy - @Autowired does not work
                        JOB_NAME, configProps.getInitialDelayMs(),
                        configProps.getPeriodMs(), TimeUnit.MILLISECONDS
                );
            }
        }
    }

    private boolean shouldScheduleJobByJobName(@NonNull final String jobName) {
        return getExecService()
                .getAllScheduledFutures().values().stream()
                .flatMap(List::stream)
                .noneMatch(scheduledFuture -> jobName.equals(scheduledFuture.getHandler().getTaskName()));
    }

    /**
     * Currently we're forced to verifying task scheduled status by scheduler name instead of task name.
     */
    private boolean shouldScheduleJobBySchedulerName(@NonNull final String schedulerName) {
        return getExecService()
                .getAllScheduledFutures().values().stream()
                .flatMap(List::stream)
                .noneMatch(scheduledFuture -> schedulerName.equals(scheduledFuture.getHandler().getSchedulerName()));
    }

    private IScheduledExecutorService getExecService() {
        return hz.getScheduledExecutorService(HZ_SCHEDULER);
    }
}