package hz.bug.app.service.scheduler;

import com.hazelcast.spring.context.SpringAware;
import hz.bug.app.service.DummyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.Serializable;

import static java.lang.System.currentTimeMillis;

/**
 * Note its dependency ({@link DummyService}) won't get autowired if
 * we're created as a NamedTask.
 */
@Slf4j
@SpringAware
public class DummyTask implements Runnable, Serializable {

    private static long serialVersionUID = 1L;

    @Autowired
    private transient DummyService dummyService;

    @Override
    public void run() {
        if (dummyService == null) {
            log.error("dummyService NOT wired in");
        } else {
            dummyService.doSomething("current time is " + currentTimeMillis());
        }
    }
}